import {  NumbersValidator  } from "../../app/numbers_validator.js";
import { expect } from "chai";
import { describe, beforeEach, afterEach, it } from "mocha";

describe('isNumberEven', function () {

  let validator;


  beforeEach(function () {
    validator = new NumbersValidator();
  });

  afterEach(function () {
    validator = null;
  });


  it('should return true if number is even', function () {
    expect(validator.isNumberEven(4)).to.be.equal(true);
  });

  it('should throw an error when provided a string', () => {
    expect(() => {
        validator.isNumberEven('4');
    }).to.throw('[4] is not of type "Number" it is of type "string"');
  })

  it('should return false if number is odd', function () {
    expect(validator.isNumberEven(5)).to.be.equal(false);
  });

  it('should return even numbers from array', function () {
    expect(validator.getEvenNumbersFromArray([2, 3, 4])).to.have.members([ 2, 4 ]);
  });

  it('should return empty array when no even numbers in the array', function () {
    expect(validator.getEvenNumbersFromArray([1, 3, 5])).to.have.members([ ]);
  });

  it('should throw an error when input is not an array of numbers', function () {
    expect(() => {
      validator.getEvenNumbersFromArray(['2', '3', '4']);
    }).to.throw('[2,3,4] is not an array of "Numbers"');
  });

  it('should throw an error when array contains a mix of numbers and strings', function () {
    expect(() => {
      validator.getEvenNumbersFromArray(['2', '3', '4']);
    }).to.throw('[2,3,4] is not an array of "Numbers"');  
  });

  it('should return true if every element of array is of type number', function () {
    expect(validator.isAllNumbers([1, 2, 0.5, -100])).to.be.equal(true);
  });

  it('should return false if every element of array is not of type number', function () {
    expect(validator.isAllNumbers(['abc', 2])).to.be.equal(false);
  });

  it('should throw an error when argument is not array', function () {
    expect(() => {
      validator.isAllNumbers('abc');
    }).to.throw('[abc] is not an array');  
  });

  it('should return true if argument is of type number', function () {
    expect(validator.isInteger(2)).to.be.equal(true);
  });

  it('should return true if argument is of type number', function () {
    expect(validator.isInteger(-2)).to.be.equal(true);
  });

  it('should return false if argument is a decimal number', function () {
    expect(validator.isInteger(0.2)).to.be.equal(false);
  });

  it('should throw an error when argument is not a number', function () {
    expect(() => {
      validator.isInteger('1');
    }).to.throw('[1] is not a number');  
  });

})